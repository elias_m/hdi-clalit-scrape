const puppeteer = require('puppeteer');

(async event => {
    const key_words = 'JavaScript';
    const link = 'https://www.clalit.co.il/he/sefersherut/pages/services_guide.aspx?q=%2A&service=clinics';

    const browser = await puppeteer.launch({ headless: false, slowMo: 100, devtools: false });

    try {
        const page = await browser.newPage();

        await page.setViewport({ width: 1199, height: 900 });

        await page.goto(link);



        //await page.waitForSelector('#lobby_search div div div fieldset:nth-child(1) div div.col-9.col-md-11.col-lg-10.col-xl-11 div:nth-child(1) input');
        //await page.click('#lobby_search div div div fieldset:nth-child(1) div div.col-9.col-md-11.col-lg-10.col-xl-11 div:nth-child(1) input');
        //await page.keyboard.type('*');
        //await page.keyboard.press('Enter');

        await page.waitFor(3000);

        await page.waitForSelector('#search_results_list');
        // Clinic Rows
        const clinicRows = await page.$$("#search_results_list > div > ul > li");
        for (var i = 0; i < clinicRows.length; i++) {
            // console.log('clinicRow: ', clinicRows[i]);
            const titleVal = await clinicRows[i].$eval('div.col-12.col-lg-7 > div.result-list-item-info > h3', el => el.innerText);
            const addressVal = await clinicRows[i].$eval('div.col-12.col-lg-7 > div.result-list-item-info > p:nth-child(5) > span.result-list-item-info-value', el => el.innerText);
            console.log(`Item: ${titleVal} - Address: ${addressVal}`);
        }

        await page.click('#search_results_list > div > nav > ul > li:nth-child(7) > a');
        await page.waitForNavigation();

        /*
        const url = await getHref(
            page,
            `#main > div #center_col #search > div > div > div a`
        );

        await page.goto(url, { waitUntil: 'domcontentloaded' });

        await page.screenshot({
            fullPage: true,
            path: 'new_image.png'
        });
        const screenshotPath = process.cwd() + '/new_image.png';

        console.log('URL of the page:', url);
        console.log('Location of the screenshot:', screenshotPath);
        */

        // await page.close();
        // await browser.close();
    } catch (error) {
        console.log(error);
        await browser.close(); 
    }
})();

const getHref = (page, selector) =>
    page.evaluate(
        selector => document.querySelector(selector).getAttribute('href'),
        selector
    );